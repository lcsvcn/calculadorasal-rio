from pdf import upworkPDF

def calculadoraUpwork(budget, upwork_tax, agency_tax, team_size):
    return (budget - (budget * upwork_tax) - (budget * agency_tax) - (2 / team_size))


def calculadoraUpworkAll(budget, upwork_tax, agency_tax, team_size, invoice_id, developer_name):
    print("Valor para o Desenvolvedor %s: %s USD" % (developer_name, round(calculadoraUpwork(budget, upwork_tax, agency_tax, team_size), 3)))
    print("Valor da taxa de Retirada: %s USD" % float(2 / team_size))
    print("Valor da taxa do Upwork: %s USD" % round((budget * upwork_tax), 3))
    print("Valor da Indicação da Agência: %s USD" % round((budget * agency_tax), 3))
    print("Valor Total Bruto: %s USD" % round((budget), 3))

    upwork_pdf = upworkPDF()
    upwork_pdf.passData(budget, round((budget * agency_tax), 3), invoice_id, developer_name)
    upwork_pdf.generatePDF()


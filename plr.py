def calculadoraPLR(salario, bonus, lider):
    extra = salario * bonus
    extra_lider = salario * bonus * 0.25

    if(lider):
        total = extra + extra_lider
    else:
        total = extra

    pormes = total / 12
    return ("Valor Salário Real: %s/mês" % (salario + int(pormes)))

def calculadoraPLRAll(salario):
    print("Sem bonus : %s" % calculadoraPLR(salario, 0, True))
    print("bonus 4x : %s" % calculadoraPLR(salario, 4, True))
    print("bonus 8x : %s" % calculadoraPLR(salario, 8, True))
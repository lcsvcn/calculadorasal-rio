from fpdf import FPDF
from datetime import date

class upworkPDF(FPDF):
    budget = 0
    agency_value = 0
    invoice_id = 0

    def passData(self, budget, value, invoice_id, developer_name):
        self.agency_value = value
        self.budget = budget
        self.invoice_id = invoice_id
        self.developer_name = developer_name

    def generatePDF(self):
        pdf = FPDF()

        pdf.alias_nb_pages()
        pdf.add_page()

        # Logo
        pdf.image('upwork.png', 10, 8, 33)
        # Arial bold 15
        pdf.set_font('Arial', 'B', 15)
        # Move to the right

        pdf.ln(20)
        pdf.cell(130)
        # Title

        pdf.cell(60, 10, 'Invoice', 1, 2, 'C')

        pdf.set_font('Arial', '', 10)
        pdf.cell(30, 10,"", 0, 2)
        pdf.cell(30, 5, 'INVOICE                  #%s' % self.invoice_id, 0, 2, 'c')
        pdf.cell(30, 5, 'DATE:                       %s' % date.today() , 0, 2, 'c')

        pdf.set_font('Arial', 'B', 10)
        pdf.cell(30, 5, 'TOTAL AMOUNT             $%s'% self.agency_value, 0, 1, 'c')

        pdf.set_font('Arial', 'B', 12)
        pdf.cell(20, 10, 'From:', 0, 0)
        pdf.set_font('Arial', '', 12)
        pdf.cell(100, 3, '', 0, 2, 'l')
        pdf.cell(100, 5, 'Upwork Global Inc.', 0, 2, 'l')
        pdf.cell(100, 5, '2625 Augustine Dr, Suite 601', 0, 2, 'l')
        pdf.cell(100, 5, 'Santa Clara CA 95054', 0, 2, 'l')
        pdf.cell(100, 5, 'USA', 0, 2, 'l')

        pdf.ln(5)

        pdf.set_font('Arial', 'B', 12)
        pdf.cell(20, 10, 'Bill to:', 0, 0)
        pdf.set_font('Arial', '', 12)
        pdf.cell(100, 3, '', 0, 2, 'L')
        pdf.cell(100, 5, 'Vieira Agency', 0, 2, 'L')
        pdf.cell(100, 5, 'Attn: Lucas Nicolau', 0, 2, 'L')
        pdf.cell(100, 5, 'Avenida Aquidaban, 1', 0, 2, 'L')
        pdf.cell(100, 5, 'Sao Carlos, 13026-510', 0, 2, 'L')
        pdf.cell(100, 5, 'Brazil', 0, 2, 'L')


        pdf.ln(20)

        pdf.set_font('Arial', '', 10)

        pdf.cell(0, 5, 'DESCRIPTION / MEMO', 1, 1, 'C')

        pdf.multi_cell(0, 5, '\n  Agency Fee to %s\n    Client: CaptionCall\n    Contract title: App developer to make an Android phone call recording app\n    $%s USD x 35%% = $%s USD\n    Notes: Invoice from Upwork for #%s\n ' % (self.developer_name, self.budget, self.agency_value, self.invoice_id), 1, 2, 'L')
        #pdf.multi_cell(150, 5, 'Client: CaptionCall', 0, 2, 'L')
        #pdf.multi_cell(150, 5, 'Contract title: App developer to make an Android phone call recording app', 0, 2, 'L')
        #pdf.multi_cell(150, 5, '$%s USD x 35%% = $%s USD' % (self.budget, self.agency_value), 0, 2, 'L')
        #pdf.multi_cell(150, 5, 'Notes: Invoice from Upwork for #%s' % self.invoice_id, 0, 1, 'L')


        pdf.set_font('Arial', 'B', 10)

        pdf.cell(0, 10, 'TOTAL AMOUNT: %s USD' % self.agency_value, 1, 0, 'C')
        pdf.output('generated.pdf', 'F')